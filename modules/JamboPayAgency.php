<?php

    namespace jambopay;

    use ICanBoogie\DateTime;

    //@ Initialize a session where necessary
    @session_start();

    class JamboPayAgency 
    {

        private static  $credentials;
        public  static  $host;     
        private static  $now;   
        private static  $token;
        public  static  $status;
        private static  $retval = true;

        public function __construct( $credentials = [], $api_host = "http://192.168.11.22/jambopayservices" )
        {

            // echo "--> <pre> JamboPayAgency   Called<br>\n";

            //@Set the credentials from the session where necessary and update the session
            $credentials = $_SESSION["jp_credentials"] =  ( @count(@$credentials) > 0) ? @$credentials : ( ( @count(@$_SESSION["jp_credentials"])  > 0 ) ? @$_SESSION["jp_credentials"] : [] );
            
            // echo "--> Credentials set<br>\n";

            //@ Ensure that the access token is available (where applicable)
            $this->token = ( @$_SESSION["jp_token"] ) ? $_SESSION["jp_token"] : NULL;
            
            // echo "--> Token set<br>\n";

            //@ Set the class instanciation time 
            $this->now  = time();

            // echo "--> Time set to {$this->now}<br>\n";

            //@ Validate the credentials (ensuring that the required parameters exist)
            if(  !$this->isDefined( @$credentials, ["username","password","app_key"] ) )
            {
                echo ( $this->make_response( 500, 'Please define an array with your username,password and app_key as your first parameter for class instantiation', $credentials ) );
                exit;
            }
            else
            {

                //@ Ensure that a grant type is defined & update the session
                $credentials["grant_type"] = $_SESSION["jp_credentials"]["grant_type"] =  (@$credentials["grant_type"]) ? $credentials["grant_type"] : ( (@$_SESSION["jp_credentials"]["grant_type"]) ? $_SESSION["jp_credentials"]["grant_type"] :  "agency" );
                // echo "--> Credentials grant_type set to {$credentials['grant_type']}<br>\n";

            }

            //@Ensure that an api endpoint is set and that it is in the proper format
            $this->host =  $_SESSION["jp_host"] =  ( $api_host == strtolower("http://192.168.11.22/jambopayservices") ) ? ( (@$_SESSION["jp_host"]) ?  $_SESSION["jp_host"] :  $api_host ) : (  ( preg_match("/\/jambopayservices/i", $api_host) ) ?  preg_replace( "/\/$/i", "", preg_replace("/\/jambopayservices/i", "", $api_host))."/jambopayservices"  : preg_replace( "/\/$/i", "", $api_host)."/jambopayservices" );
            // echo "--> Host set to {$this->host}<br>\n";

            //@ Check if the user is loged in
            $this->is_login_valid();
            // echo "--> Login validation completed<br>\n";
            
        }


        //@ Handle the setting of the host url
        public function set_endpoint_url ( $url )
        {
            self::$host =  $_SESSION["jp_host"] =  ( $api_host == strtolower("http://192.168.11.22/jambopayservices") ) ? ( (@$_SESSION["jp_host"]) ?  $_SESSION["jp_host"] :  $api_host ) : ( ( preg_match("/\/jambopayservices/i", $api_host) ) ?  preg_replace( "/\/$/i", "", preg_replace("/\/jambopayservices/i", "", $api_host))."/jambopayservices"  : preg_replace( "/\/$/i", "", $api_host)."/jambopayservices" );
        }

        //@ Handle the authentication and re_authentication of users
        private function is_login_valid()
        {

            // echo "--> Login validation started<br>\n";

            //@ Check the session variables for session and token validity
            if(  self::isDefined($_SESSION, ["jp_token","jp_credentials","jp_issued","jp_expires"])  )
            {
                // echo "--> The required session parameters are defined {$_SESSION['jp_issued']}<br>\n";

                //@Format the issued and expired session values to proper dates
                $issued     = new DateTime($_SESSION["jp_issued"]);                
                $expires    = new DateTime($_SESSION["jp_expires"]);
                $expires    = $expires->format("U");

                // // echo "--> <br>\n";

                //@ Set the login status
                self::$status = [
                    "now"       => self::$now,
                    "expires"   => $expires,
                    "expired"   => ( self::$now >= $expires) ? true : false
                ];
                // echo "--> Application's validity status defined<br>\n";

                //@ Ensure that the current token is valid
                if( self::$status["expired"]  )
                {
                    // echo "--> Attempting to renew an expired token<br>\n";
                    //@ Renew the token
                    self::Login();
                }
                else
                {
                    //@ ALL GOOD TO GO
                    // echo "--> Your token is still valid, proceed<br>\n";
                }


            }
            //@ Perform a login
            else
            {
                // echo "--> Attempting to log you in <br>\n";
                self::Login();
            }


        }

        //@ Perform a Login
        private function Login( ) 
        {
           
            //@ Ensure that the required  parameters are all defined
            if( self::isDefined($_SESSION["jp_credentials"], ["username","password","grant_type","app_key"]) )
            {

                //@ Attempt an actual login
                $login_response = self::remote("/token","POST", [ 
                                                                        "username"      => $_SESSION["jp_credentials"]["username"], 
                                                                        "password"      => $_SESSION["jp_credentials"]["password"], 
                                                                        "grant_type"    => $_SESSION["jp_credentials"]["grant_type"]  
                                                                    ],
                                                                    [
                                                                        "app_key"       => $_SESSION["jp_credentials"]["app_key"]
                                                                    ]);
                

                //@ Check if the attempt was successful
                if( @$login_response["status"] == 200  )
                {

                    //@ Ensure that the returned response is in array format
                    $login_response["response"] = ( is_array($login_response["response"]) ) ? $login_response["response"] : json_decode($login_response["response"],1);

                    //@ UPDATE/SET the necessary session variables
                    self::$token = $_SESSION["jp_token"]   = $login_response["response"]["access_token"];
                    $_SESSION["jp_issued"]  = $login_response["response"][".issued"];
                    $_SESSION["jp_expires"] = $login_response["response"][".expires"];

                }
                //@ Inform the user of the failed login attempt
                else
                {

                    echo ( self::make_response( $login_response["status"], "An attempt to login to {$_SESSION['jp_host']} failed with the attached error", $login_response["response"] ) );
                    exit;

                }

                    
            
            }
            //@ Inform the user of undefined credentials
            else
            {
                echo ( self::make_response( 412, "Please ensure that you have defined a username, password and grant type for access", $_SESSION["jp_credentials"]) );
                exit;
            } 

        }


        //@ Force a logout
        private function Logout()
        {
            @session_destroy();
            echo self::make_response(200, "You have been logged out of the Jambopay service.");
            exit;
        }

        //@ Format the request
        public function remote( $target, $method = "GET", $body = [], $headers = ["content-type" => "application/x-www-form-urlencoded"] )
        {
    
            
            $method = strtoupper( $method  );
    
            switch ($method) {
                case 'POST':
                case 'PUT':
                case 'DELETE':
                   
                    //@ Ensure that the required headers are defined
                    $headers["content-type"]   = 'application/x-www-form-urlencoded';
                    $headers["app_key"]        = (@$headers["app_key"]) ? $headers["app_key"] : $_SESSION["jp_credentials"]["app_key"];
                    $headers["Authorization"]  = (@$headers["Authorization"]) ? $headers["Authorization"] : "Bearer ".((@$_SESSION["jp_token"]) ? @$_SESSION["jp_token"] : self::$token);

                    //@ Capture the constituent keys of the body object
                    $kys = array_keys(@$body);

                    //@ Filter to check for any objects that might be arrays
                    $arrs =  array_filter( $kys, function( $ky ) { return is_array(@$body[$ky]); } );

                    //@ Remove any null - valued arrays
                    $arrs  =  array_filter( $arrs, function( $k ){ return (@$body[$k] != NULL &&  @$body[$k] != "");  } );
    
                    //@ Loop through each 'surviving' refrence key in the array object 
                    forEach( $arrs as $a )
                    {  
    
                        //@ loop through for as many keys as are in the array
                        foreach ( $body[$a] as $body_a_index => $value_at_body_a ) {
                            
                            //@ Capture the Object keys at the relevant index
                            $obj_kys = array_keys($body[$a][$body_a_index]);
    
                            //@ Perform only if the object at this position is typeof 'Array' (using the crude check)
                            if( $obj_kys[0] != 0 )
                            {
    
                                //@ Loop through each available object key
                                foreach ($obj_kys as $obj_ky) {
                                   
                                    //@ Populate the body object with the given parameter in the desired format
                                    $body["{$a[$body_a_index][$obj_ky]}"] = $body[$a][$body_a_index][$obj_ky];
    
                                }
      
    
                            }
    
    
                        }
    
                        unset( $body[$a] );                                      
                       
                    }

                    return  self::request( [
                        "url"           => "{$_SESSION["jp_host"]}{$target}",
                        "method"        => $method,
                        "parameters"    => $body,
                        "headers"       => $headers
                    ]);

                    
                break;
                
                case "GET":
    
                    //@ Ensure that the required headers are defined
                    $headers["content-type"]   = 'application/x-www-form-urlencoded';
                    $headers["app_key"]        = (@$headers["app_key"]) ? @$headers["app_key"] : @$_SESSION["jp_credentials"]["app_key"];
                    $headers["Authorization"]  = (@$headers["Authorization"]) ? $headers["Authorization"] : "Bearer ".((@$_SESSION["jp_token"]) ? @$_SESSION["jp_token"] : @self::$token);


                    return  self::request( [
                        "url"           => @"{$_SESSION["jp_host"]}{$target}",
                        "parameters"    => $body,
                        "headers"       => $headers,
                        "method"        => $method
                    ]);
                    // exit;
    
                break;
            
                default:
                    echo self::make_response( 500, "An unsupported http verb ({$method}) was specified for use with Jambopay.");
                    exit;
                break;
            }
    
        }
    
        //@ Format the request
        public function custom_remote( $target, $method, $body, $headers = ["content-type" => "application/x-www-form-urlencoded"] )
        {
    
            $method = strtoupper( $method  );
    
            switch ($method) {
                case 'POST':
                case 'PUT':
                case 'DELETE':
                   
                    //@ Ensure that the required headers are defined
                    $headers["content-type"]   = 'application/x-www-form-urlencoded';                     
                    $headers["app_key"]        = (@$headers["app_key"]) ? $headers["app_key"] : $_SESSION["jp_credentials"]["app_key"];
                    $headers["Authorization"]  = (@$headers["Authorization"]) ? $headers["Authorization"] : "Bearer ".((@$_SESSION["jp_token"]) ? @$_SESSION["jp_token"] : self::$token);

                    //@ Capture the constituent keys of the body object
                    $kys = array_keys(body);

                    //@ Filter to check for any objects that might be arrays
                    $arrs =  array_filter( $kys, function( $ky ) { return is_array(@$body[$ky]); } );

                    //@ Remove any null - valued arrays
                    $arrs  =  array_filter( $arrs, function( $k ){ return (@$body[$k] != NULL &&  @$body[$k] != "");  } );
    
                    //@ Loop through each 'surviving' refrence key in the array object 
                    forEach( $arrs as $a )
                    {  
    
                        //@ loop through for as many keys as are in the array
                        foreach ( $body[$a] as $body_a_index => $value_at_body_a ) {
                            
                            //@ Capture the Object keys at the relevant index
                            $obj_kys = array_keys($body[$a][$body_a_index]);
    
                            //@ Perform only if the object at this position is typeof 'Array' (using the crude check)
                            if( $obj_kys[0] != 0 )
                            {
    
                                //@ Loop through each available object key
                                foreach ($obj_kys as $obj_ky) {
                                    
                                    // console.log( `${obj_ky} => ${body[a][0][obj_ky]}` )
                                    //@ Populate the body object with the given parameter in the desired format
                                    $body["{$a[$body_a_index][$obj_ky]}"] = $body[$a][$body_a_index][$obj_ky];
    
                                }
      
    
                            }
    
    
                        }
    
                        // console.log(obj_kys)
                        unset( $body[$a] );                                      
                       
                    }
    
                    return  self::request( [
                        "url"           => $target,
                        "method"        => $method,
                        "parameters"    => $body,
                        "headers"       => $headers
                    ]);
    
                break;
                
                case "GET":
    
                    //@ Ensure that the required headers are defined
                    $headers["content-type"]   = 'application/x-www-form-urlencoded';
                    $headers["app_key"]        = (@$headers["app_key"]) ? $headers["app_key"] : $_SESSION["jp_credentials"]["app_key"];
                    $headers["Authorization"]  = (@$headers["Authorization"]) ? $headers["Authorization"] : "Bearer ".  (@$_SESSION["jp_token"]) ? @$_SESSION["jp_token"] : self::$token;

                    return  self::request( [
                        "url"           => $target,
                        "parameters"    => $body,
                        "headers"       => $headers,
                        "method"        => $method
                    ]);
    
                break;
            
                default:
                    echo  self::make_response( 500, "An unsupported http verb ({$method}) was specified for use with Jambopay.");
                    exit;
                break;
            }
    
        }

        //@ HANDLES CURL REQUESTS TO JP _ MIDDLEWARE _ SERVICES
        public function request ( $request_parameters = [] )
        {

            //@ ensure that the required parameters are defined
            if( self::isDefined( $request_parameters, ["url","parameters","headers","method"] )  )
            {

                $curl = curl_init();

                curl_setopt_array($curl, array(
                    CURLOPT_URL             => ( $request_parameters["method"] == "GET" ) ?  $request_parameters['url']."?".http_build_query($request_parameters['parameters']) : $request_parameters['url'],
                    CURLOPT_FOLLOWLOCATION  => true,
                    CURLOPT_RETURNTRANSFER  => true,
                    CURLOPT_VERBOSE         => 0,
                    CURLOPT_HEADER          => 0,
                    CURLOPT_MAXREDIRS       => 10,
                    CURLOPT_TIMEOUT         => 30,
                    CURLOPT_HTTP_VERSION    => CURL_HTTP_VERSION_1_1,
                    CURLOPT_SSL_VERIFYPEER  => 0,
                    CURLOPT_SSL_VERIFYHOST  => 0,
                    CURLOPT_CUSTOMREQUEST   => @strtoupper(@$request_parameters["method"]),
                    CURLOPT_POST            => true,
                    CURLOPT_POSTFIELDS      => @http_build_query(@$request_parameters["parameters"]),
                    CURLOPT_HTTPHEADER      => self::formatHeaders( @$request_parameters["headers"] ),
                ));

                $json = curl_exec($curl);
                $code = curl_getinfo($curl,CURLINFO_HTTP_CODE);
                // $info = curl_getinfo($curl);
                curl_close($curl);

                return  [ 
                    "status"    => $code,
                    "response"  => $json,                    
                ];
                // "info"      => $info,
                // "query"     => [
                //     "headers"   => self::formatHeaders( @$request_parameters["headers"]),
                //     "request"   => @strtoupper(@$request_parameters["method"]),
                //     "fields"    => @http_build_query(@$request_parameters["parameters"])
                // ]


            }
            //@ Let the user know that there were missing parameters in the passed request
            else
            {

                echo self::make_response(412,"The request method expects a url,parameters,headers and a method to be defined",$request_parameters);
                exit;                                                                                              

            }
            

        }


        //@ Handle the response format
        public function make_response ( $status = "200", $message = "All Seems Good", $command = "continue" )
        {
            return json_encode([
                "status"    => $status
                ,"data"     => [
                    "message"   =>  $message,
                    "command"   =>  $command
                ]
            ]);
        }

         //@ Process the checking for specified parameters
         private function is_defined_process( $object, $paramsArray )
         {
             
             foreach( $paramsArray as $param )
             {
     
                 if( !array_key_exists($param, $object) )
                 {
                    self::$retval = false;
                 }
     
             }
     
             return self::$retval;
     
         }
     
         // @ Checks if the specified keys exist and have a value in a given md=>array
         public function isDefined ( $object, $paramsArray )
         {
     
             $paramsArray = ( is_array($paramsArray) ) ? $paramsArray : explode( ",", $paramsArray );
     
             self::$retval = true;
     
             if ( !$paramsArray )
             {
                 return false;
             }
             else
             {
                 return self::is_defined_process($object,$paramsArray);
             }
     
         }  


         public function formatHeaders ( $header_array = [] )
         {

            $response_array = [];

            foreach ($header_array as $key => $value) {
               array_push($response_array, "{$key}: $value" );
            }

            return $response_array;

         }

    }

?>