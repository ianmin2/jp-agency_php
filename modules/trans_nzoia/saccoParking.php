<?php

    namespace jambopay\tnc;

    class SaccoParking extends JambopayGeneric
    {

        public $MerchantID;

        public function __construct (  )
        {
            parent::__construct( "saccoparking" );
            $this->MerchantID   = "TRANS";
        }


        public function get_saccos ( $headers = [] )
        {

            $params = [
                "Stream"     => self::$Stream,
                "MerchantID" => self::$MerchantID
            ];

            return (self::remote('/api/SaccoParking/GetSaccos','GET', $params, $headers));

        } 


        //# Expects { }
        public function get_sacco_parking_details ( $headers = [] )
        {

            $params = [
                "Stream"     => self::$Stream,
                "MerchantID" => self::$MerchantID
            ];

           return (self::remote('/api/payments/GetSaccoParkingDetails','GET', $params, $headers));

        }

        public function prepare_payment ( $BillData = [] , $headers = [] )
        {
        
            $BillData["Stream"]         = self::$Stream;
            
            $BillData["PaymentTypeID"]  = ( @$BillData["PaymentTypeID"] != NULL  ) ? $BillData["PaymentTypeID"] : 1;
            
            $BillData["MerchantID"]     = self::$MerchantID;
    
            return (self::remote('/api/payments/Post','POST', $BillData, $headers));
        
        }
        
        public function commit_payment ( $BillData = [], $headers = [] )
        {
            
            $BillData["Stream"]         = self::$Stream ;
            $BillData["PaymentTypeID"]  = ($BillData["PaymentTypeID"]) ? $BillData["PaymentTypeID"] : 1;
            $BillData["MerchantID"]     = self::$MerchantID;

            return (self::remote('/api/payments/Put','PUT', $BillData, $headers));

        }
    
    }

?>