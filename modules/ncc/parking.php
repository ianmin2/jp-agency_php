<?php

    namespace jambopay\ncc;

    use jambopay\JambopayGeneric;

    class Parking extends JambopayGeneric {

        public function __construct () 
        {        
          // echo "--> Class ncc\Parking Called<br>\n";
            parent::__construct("parking");
        }

        //# Expects { }
        public function get_daily_parking_items ( $headers = [] )
        {
            return (self::remote('/api/payments/GetDailyParkingItems','GET', ["Stream" => self::$Stream], $headers));
        }

        //# Expects { }
        public function get_seasonal_parking_items ( $headers = [] )
        {

            return (self::remote('/api/payments/GetSeasonalParkingItems','GET', ["Stream"=>self::$Stream], $headers));

        }

    }

?>