<?php 

    namespace jambopay\ncc;

    class UBP extends JambopayGeneric {

        public function __construct () {
            parent::__construct("sbp");
        }

        //@Expects {PhoneNumber,ActivityID}
        public function get_sbp_classes(  $params = [], $headers = [] )
        {

            $params["Stream"] = self::$Stream;

            return (self::remote('/api/payments/getsbpclasses','GET', $params , $headers));

        }

        //@Expects {PhoneNumber,ID}
        public function get_sbp_sub_classes(  $params = [], $headers = [] )
        {
    
            $params["Stream"] = self::$Stream;    
            
            return (self::remote('/api/payments/getsbpsubclasses','GET', $params , $headers));
    
        }

        //@Expects {PhoneNumber,subclassID}
        public function get_sbp_activity_charges(  $params = [], $headers = [] )
        {

            $params["Stream"] = self::$Stream;
            
            return (self::remote('/api/payments/GetSBPActivityCharges','GET', $params , $headers));

        }

        //@Expects {}
        public function get_sbp_activity_sub_counties( $headers = [] )
        {
            
            return (self::remote('/api/payments/getsbpsubcounties','GET', ["Stream"=> self::$Stream] , $headers));

        }

        //@Expects {id}
        public function get_sbp_wards(  $params = [], $headers = [] )
        {

            $params["Stream"] = self::$Stream;
            
            return (self::remote('/api/payments/getsbpwards','GET', $params , $headers));

        }

        //@Expects {LicenseID,Year,PhoneNumber}
        public function get_business(  $params = [], $headers = [] )
        {

            $params["Stream"] = self::$Stream;
            
            return (self::remote('/api/payments/GetBusiness','GET', $params , $headers));

        }

        //@Expects {ActivityID,PhoneNumber}
        public function get_business_class_id(  $params = [], $headers = [] )
        {

            $params["Stream"] = self::$Stream;
            
            return (self::remote('/api/payments/GetSBPClasses','GET', $params , $headers));

        }

        //@Expects {ID,PhoneNumber}
        public function get_business_sub_class_id(  $params = [], $headers = [] )
        {

            $params["Stream"] = self::$Stream;
            
            return (self::remote('/api/payments/getsbpsubclasses','GET', $params , $headers));

        }


    }


?>