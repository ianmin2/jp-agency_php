<?php

    namespace jambopay\ncc;

    class HouseRent extends JambopayGeneric
    {

        public function __construct (  )
        {
            parent::__construct("rent");
        }


        //# Expects { }
        public function get_estates ( $headers = [] )
        {

            return (self::remote('/api/payments/GetEstates','GET', ["Stream" => self::$Stream], $headers));

        }


        //# Expects { ID }
        public function get_residences ( $params = [], $headers = [] )
        {

            $params['Stream'] = self::$Stream;

            return (self::remote('/api/payments/GetResidences','GET', $params , $headers));

        }

    }

?>