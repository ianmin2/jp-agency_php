<?php

    namespace jambopay;
    
    class Kiwasco extends JambopayGeneric {

        public function __construct ( $params =  "kisumuwaterinvoice"  )
        {
            parent::__construct($params);
        }

        //# Expects { accountNumber, stream="kisumuwaterinvoice" }
        public function get_account_details ($accountNumber, $headers = [] )
        {
        
            $d = [
                    "accountNumber" => $accountNumber,
                    "Stream" => self::$Stream, 
                    "AccountNumber" => $accountNumber
                ];
                
            return (self::remote('/api/payments/GetAccountDetails','GET', $d, $headers));
            
        }
    
    }

?>