<?php

    namespace jambopay;
    
    class JambopayGeneric extends JamboPayAgency
    {

        public static $Stream;

        public function __construct ( $Stream )
        {
          // echo "--> JambopayGeneric Called<br>\n";
            self::$Stream = ( @$Stream ) ? $Stream : NULL;
            parent::__construct( $GLOBALS["LOGIN_PARAMS"], $GLOBALS["JAMBOPAY"] );
        }
        
       
          //# Expects { RevenueStreamID, MerchantID, Narration,  PhoneNumber, Amount, stream="merchantbill" }
        public  function bill ( $BillData = [], $headers = [] )
        {

            $BillData["Stream"]     = ( @$BillData["Stream"] ) ?  @$BillData["stream"] :  (self::$Stream) ? self::$Stream  : "merchantbill";
            
            return ( self::remote('/api/payments/Post','POST', $BillData, @$headers));

        }

        //# Expects {  BillNumber, Year, Month, stream="merchantbill", PhoneNumber}
        public  function bill_status ( $BillData = [], $headers = [] )
        {
            
            $BillData["Stream"]     = ( @$BillData["Stream"] ) ?  @$BillData["stream"] :  (self::$Stream) ? self::$Stream  : "merchantbill";
            
            return (self::remote('/api/payments/GetBill','GET', $BillData, @$headers));

        }

            
        //# Expects {  MerchantID, PhoneNumber, BillNumber, PaymentTypeID=1, stream="merchantinvoice", PhoneNumber}
        public  function get_bill ( $BillData = [], $headers = [] )
        {
        
            $BillData["Stream"]         = ( @$BillData["Stream"] ) ?  @$BillData["stream"] :  (self::$Stream) ? self::$Stream  : "merchantbill";
            $BillData["PaymentTypeID"]  = (@$BillData["PaymentTypeID"]) ? @$BillData["PaymentTypeID"] : 1;
            
            return (self::remote('/api/payments/Post','POST', $BillData, @$headers));

        }
    
        public  function prepare_payment ( $BillData = [] , $headers = [] )
        {
            
            $BillData["Stream"]         = ( @$BillData["Stream"] ) ?  @$BillData["stream"] :  (self::$Stream) ? self::$Stream  : "merchantbill";
            $BillData["PaymentTypeID"]  = (@$BillData["PaymentTypeID"]) ? @$BillData["PaymentTypeID"] : 1;
        
            return  (self::remote('/api/payments/POST','POST', $BillData, @$headers));

        }

        //# Expects {  TransactionID, Tendered, PaidBy, stream="merchantinvoice" }
        public  function pay_bill ( $BillData = [], $headers = [] )
        {

            $BillData["Stream"]         = ( @$BillData["Stream"] ) ?  @$BillData["stream"] :  (self::$Stream) ? self::$Stream  : "merchantbill";
            $BillData["PaymentTypeID"]  = (@$BillData["PaymentTypeID"]) ? @$BillData["PaymentTypeID"] : 1;
                    
            return  (self::remote('/api/payments/Put','PUT', $BillData, @$headers));

        }

        public function commit_payment ( $BillData = [], $headers = [] )
        {
        
            $BillData["Stream"]         = ( @$BillData["Stream"] ) ?  @$BillData["stream"] :  (self::$Stream) ? self::$Stream  : "merchantbill";
            $BillData["PaymentTypeID"]  = (@$BillData["PaymentTypeID"]) ? @$BillData["PaymentTypeID"] : 1;
            
            return (self::remote('/api/payments/PUT','PUT', $BillData, @$headers));

        }

        //# Expects { billNumber, stream="merchantbill" }
        public function get_merchant_streams ($merchantID = "NCC", $Stream="merchantbill", $headers = [] )
        {

            return (self::remote('/api/payments/GetMerchantStreams','GET', ["merchantID" => $merchantID,"Stream" => $Stream], @$headers));

        }

    }

?>
