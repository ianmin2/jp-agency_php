<?php

    namespace jambopay;

    class Startimes extends JambopayGeneric
    {

        public function __construct () 
        {
            parent::__construct( "startimes" );
        }

        //# Expects {  { SmartCardCode, CustomerCode } , stream="startimes" }
        public function get_transactions ( $data = [], $headers = [] )
        {
        
            $data["Stream"]     = self::$Stream;

            return (self::remote('/api/payments/GetTransactions','GET', $data, $headers));
           
        }

    }

?>