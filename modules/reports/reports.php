<?php

	namespace Jambopay;

	class Reports
	{

		public $stream;

		public function __construct(  $stream )
		{
		
			if( @$stream != NULL && @strlen(@$stream) > 0 )
			{
			
				self::$Stream = $stream;
			
			}
			else 
			{
			
				throw new Error('Please define a valid reporting stream.\n\n\te.g\t $parking_reports = new JP.reports("parking");');
			
			}

		}

		//# Expects { PlateNumber,Index,TransactionID,StartDate,EndDate,UserID,TransactionStatus }
		public function get_transactions ( $params = [] , $headers = [] )
		{
		
			$params["Stream"] = self::$Stream;

			return (self::remote('/api/payments/GetTransactions','GET', $params , $headers));

		}

	}

?>