<?php

    namespace jambopay;

    class OffStreetParking extends JambopayGeneric
    {

        public $endpointURL;

        public function __construct( $isLive = false )
        {
            $this->endpointURL = ( $isLive == TRUE ) ? "http://192.168.11.167" : "http://cashoffice.nairobi.go.ke/Offstreet";
            parent::__construct("offstreetparking");
        }

        //@ Expects {PhoneNumber}, headers
        public function validate_attendant ( $loginData = [], $headers = [] )
        {

            return (self::custom_remote(self::$endpointURL."/api/Login",'GET', $loginData, $headers));

        }

        //@ Expects {PhoneNumber,attPhoneNumber} , headers
        public function book_vehicle ( $vehicleData = [], $headers = [] )
        {

            $vehicleData["Stream"]  = self::$Stream;

            return ( self::custom_remote(self::$endpointURL."/api/Booking",'GET', $vehicleData, $headers));

        }

        //@ Expects {plateNumber} , headers
        public function get_booking_info ( $vehicleData = [], $headers = [] )
        {

            $vehicleData["Stream"]  = self::$Stream;
            
            return (self::custom_remote(self::$endpointURL."/api/Parking",'GET', $vehicleData, $headers));

        }

        //@ Expects {PlateNumber,PaidBy,ReceiptNumber,TranID} , headers
        public function payment_notification ( $vehicleData = [], $headers = [] )
        {

            $vehicleData["Stream"]  = self::$Stream;
            
            return ( self::custom_remote(self::$endpointURL."/Offstreet/api/Status",'POST', $vehicleData, $headers));

        }


    }

?>