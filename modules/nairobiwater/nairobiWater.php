 <?php 
 
    namespace jambopay;

    class NairobiWater extends JambopayGeneric
    {

        public function __construct (  )
        {
            parent::__construct("nairobiwater");
        }


        //# Expects { AccountNumber }
        public function get_bill ( $params = [], $headers = [] )
        {

            $params['Stream'] = self::$Stream;
                
            return (self::remote('/api/payments/GetBill','GET', $params , $headers));            

        }

    }

?>