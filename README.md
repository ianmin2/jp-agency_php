# __JAMBOPAY SDK FOR PHP__


> This is the unofficial php integration module for integration with the Jambopay API

All of the methods called return promises and can therefore be easily chained

## __INSTALLATION__
`Navigate to your project directory Run`
       
    composer install bixbyte\jambopay

`on your CLI.`

____
## 1. __GETTING STARTED__

    
    $GLOBALS["LOGIN_PARAMS"]  = [
        "username" => "YOUR_USERNAME", 
        "password" => "YOUR_PASSWORD",
        "app_key"  => "YOUR_JAMBOPAY_APP_KEY"
    ];


`The above sets the access credentials for use with the package`

____
## 2. SET THE ENDPOINT URL
*( `Not necessary if using 192.168.11.22 [for tests] via VPN` )*

`Example:`

    $GLOBALS["JAMBOPAY"]      = "YOUR_JAMBOPAY_URL";

____
## 3. STARTUP ()

*`Example`:*

    include( __DIR__."/index.php" );


## 3. DECLARE DEPENDENCY (*use jambopay*)

*`Example`:*

    use jambopay;



#### HEADERS PARAMETER
> This should essentialy be left blank but can be used to inject custom headers where need arises :

    `$headers` in the various methods below.

This applies for each of the methods that accepts a `headers` parameter.

____


# __USER METHODS__


___
# 1. __NAIROBI COUNTY COUNCIL PAYMENTS (`jambopay\ncc`)__

`Instantiate the class`

    $ncc_ubp = new jambopay\ncc\UBP();

____

## A). *UNIVERSAL BUSINESS PERMIT (`jambopay\ncc\UBP()`)*

### **`UBP REGISTRATION`**    
##### *Available methods*
____  

#### a). __*`.get_sbp_classes({PhoneNumber,ActivityID},$headers)`*__
> This method gets all the business categories for all businesses as classified by the Nairobi County Government.

> The *`ActivityID`* (basically a class/category into which the business falls) in the response will be used on the *`.get_sbp_sub_classes`* call. 

*`Example`:*


    $ncc_ubp->get_sbp_classes([ 
        "PhoneNumber" => "...",
        "ActivityID"  => "..."
    ]);
    
___


#### b). __*`.get_sbp_sub_classes({PhoneNumber,ID},$headers)`*__
> The above request gets you the business sub-Classes related to the Activity ID.

> The *`sub-ClassID`* on the response will be used in the *`.get_sbp_activity_charges`* call.

*`Example`:*

    $ncc_ubp->get_sbp_sub_classes([ 
        "PhoneNumber" => "...",
        "ID"          => "..."
    ]);   

___


#### c). __*`.get_sbp_activity_charges({PhoneNumber,subclassID},$headers)`*__
> The *`ChargeID`* parameter in the response can be used as a filtering parameter when making subsequent calls

*`Example`:*

    $ncc_ubp->get_sbp_activity_charges([ 
        "PhoneNumber" => "...",
        "subclassID"  => "..."
    ]);


___


#### d). __*`.get_sbp_activity_sub_counties($headers)`*__

> This call lists all the sub-counties within Nairobi county. The client chooses this and the subsequent call in order to help determine his/her business’ approximate location.

*`Example`:*

    $ncc_ubp->get_sbp_activity_sub_counties();

___


#### e). __*`.get_sbp_wards({id},$headers)`*__
> This call lists all the wards within a given Sub-county in the County.

*`Example`:*

    $ncc_ubp->get_sbp_wards([ 
            "id" => "..."
    ]);

___


#### f). __*`.prepare_payment({Year,Names,PhoneNumber,PhonePin,IDNumber,ActivityCode,ActivityName,SBPSubClassID,RelativeSize,WardName,ZoneName,AcceptedTnC,WardCode,ZoneCode,BusinessName,Pin,IDDocumentNumber,BusinessClassificationDetails,PremisesArea,NumberOfEmployees,AnnualSBPAmount,PostalCode,Town,Telephone1,Telephone2,FaxNumber,Email,PhysicalAddress,PlotNumber,Building,BuildingCategory,Floor,RoomStallNumber,ContactPersonName,ContactPersonDesignation,ContactPersonPOBox,ContactPersonPostalCode,ContactPersonTown,ContactPersonTelephone1,ContactPersonTelephone2,PaidBy,ApprovalStatus,PaymentTypeID,SBPPaymentType	}, headers)`*__

> Acceptable Parameters

| Parameter | Type | Required | Description 
| :--- | :--- | :--- | :--- |
|BusinessID |String| - |
|Year |Int |`R`|
|TransactionID| String |-|
|Names |String| `R` |Isaac Juma
|BillNumber |String |-|
|BillStatus |String |-|
|PhoneNumber |String| `R`| 254724659560
|PhonePin |Int |`R` |1234
|IDNumber |String |`R` |1
|ActivityCode |Int| `R`
|ActivityName |String |`R` |Painting
|SBPSubClassID |Int |`R` |1114
|RelativeSize |String |`R` |1
|WardName |String |`R`|
|ZoneName |String| `R`|
|AcceptedTnC |Boolean| `R` |1
|WardCode |Int |`R`| 4
|ZoneCode |Int| `R` |136
|BusinessName |String| `R` |Dcyntech
|BusinessRegistrationNumber |String |-
|Pin |String |`R` |JJITYTU767868|
|VAT |String| -| JHJH2323
|IDDocumentNumber |String| `R` |28734420
|BusinessClassificationDetails| String| `R` |1212
|OtherBusinessClassificationDetails |String| - |2121
|PremisesArea |String| `R` |121
|NumberOfEmployees| Int |`R`| 121212
|AnnualSBPAmount| Double| -| 0
|POBox| String| `R`| 8782 Nairobi
|PostalCode| String |`R`| 00100
|Town| String| `R`| Nairobi
|Telephone1| String| `R` |254717803383
|Telephone2| String| `R` |254717803383
|FaxNumber| String| - |0717803383
|Email| String| `R`| ijuma@webtribe.com
|PhysicalAddress| String| `R`| dsfsdfsd
|PlotNumber| String| `R`| 343ffdf
|Building| String| `R` |34634
|BuildingCategory| String| `R`| Non-Storey
|Floor| String| `R` |n\/a
|RoomStallNumber| String| `R` |12
|ContactPersonName| String| `R` |Simiyu Robert
|ContactPersonDesignation| String| `R`
|ContactPersonPOBox| String| `R` |281 Soy
|ContactPersonPostalCode |String |`R`| 30105
|ContactPersonTown| String| `R`|
|ContactPersonTelephone1 |String| `R` |0717803383
|ContactPersonTelephone2 |String |`R` |0717803383
|ContactPersonFaxNumber| String| - |3434
|PaidBy| String| `R`|SAMUEL KAHARA
|ApprovalStatus| Int| -| 0
|PaymentTypeID |Int| `R`| 1
|SBPPaymentType| Int| `R` |1
|ReceiptNumber |String| -|
|LocalReceiptNumber| String |-|
|ReferenceNumber| String| -|
|BankName| String| -|
|BranchName| String| -|
|Exclusions[0].ID| String| - |ChargeID excludedfrom payment 
|Exclusions[1].ID| String| - |ChargeID excluded from payment
|

> The *`transactionID`* in the response is used to commit the transaction on the `.commit_payment` call.


*`Example`:*

    $ncc_ubp->prepare_payment([
        "Year"            => "...",
        "Names"           => "...",
        "PhoneNumber"     => "...",
        "PhonePin"        => "...",
        "IDNumber"        => "...",
        "ActivityCode"    => "...",
        "ActivityName"    => "...",
        "SBPSubClassID"   => "...",
        "RelativeSize"    => "...",
        "WardName"        => "...",
        "ZoneName"        => "...",
        "AcceptedTnC"     => "...",WardCode,"ZoneCode,BusinessName"   => "...",
        "Pin"             => "...",
        "IDDocumentNumber"    => "...",
        "BusinessClassificationDetails"   => "...",
        "PremisesArea"    => "...",
        "NumberOfEmployees"   => "...",
        "AnnualSBPAmount" => "...",
        "PostalCode"      => "...",
        "Town"            => "...",
        "Telephone1"      => "...",
        "Telephone2"      => "...",
        "FaxNumber"       => "...",
        "Email"           => "...",
        "PhysicalAddress" => "...",
        "PlotNumber"      => "...",
        "Building"        => "...",
        "BuildingCategory"    => "...",
        "Floor"           => "...",
        "RoomStallNumber" => "...",
        "ContactPersonName"   => "...",
        "ContactPersonDesignation"    => "...",
        "ContactPersonPOBox"  => "...",
        "ContactPersonPostalCode" => "...",
        "ContactPersonTown"   => "...",
        "ContactPersonTelephone1" => "...",
        "ContactPersonTelephone2" => "...",
        "PaidBy"          => "...",
        "ApprovalStatus"  => "...",
        "PaymentTypeID"   => "...",
        "SBPPaymentType"  => "..."
    ]);

___


#### g). __*`.commit_payment({PhoneNumber,SBPPaymentType,Tendered,TransactionID,PaymentTypeID,PaidBy,PhonePin},$headers)`*__


| **Parameter**   |   **Type** |  **Required** |**Description**
| :--- | :--- | :--- |  :---
|  *MerchantID*   |    String  |  `R`  |           Merchant identifier e.g. NCC
| *PhoneNumber*   |   String  |  `R`  |           Agent Phone number
| *SBPPaymentType* |  Int    |   `R`     |        1
|  *Tendered*   |      Double   | `R`  |           Confirmation amount of the SBP total bill
|  *TransactionID* |   String |   `R`   |          Unique ID from the POST response.
|  *PaymentTypeID*  |  Int    |   `R`   |          1
|  *PaidBy*    |       String  |  `R`        |     Person Paying for the registration
|  *PhonePin*     |    String  |  `R`    |         Pin of the wallet being debited
|

> The response parameters will contain a valid *`ReceiptNumber`* which denotes that
the transaction has been successfully commited and completed.


*`Example`:*

    $ncc_ubp->commit_payment([
        "PhoneNumber"     => "...",
        "SBPPaymentType"  => "...",
        "Tendered"        => "...",
        "TransactionID"   => "...",
        "PaymentTypeID"   => "...",
        "PaidBy"          => "...",
        "PhonePin"        => "..."
    ]);


____

### **`UBP RENEWAL`**
##### *Available methods*
____  



#### a). __*`.get_business( {LicenseID,Year,PhoneNumber}, headers )`*__
> This call gets the business details for the business as registered using the `LicenseID` (*`BusinessID`*).

> The *`ActivityID`* provided in the response is to be used in the subsequent calls.


*`Example`:*

    $ncc_ubp->get_business([
            "LicenseID"       => "...",
            "Year"            => "...",
            "PhoneNumber"     => "..."
    ]);
___


#### b). __*`.get_business_class_id( {ActivityID,PhoneNumber} ,headers )`*__
> Gets the business Classes related to the `ActivityID`.

> The `ClassID` on the response will be used in the subsequent calls.


*`Example`:*

    $ncc_ubp->get_business_class_id([
        "ActivityID"      => "...",
        "PhoneNumber"     => "..."
    ]);


___

#### c). __*`.get_business_sub_class_id({ID,PhoneNumber},$headers)`*__

> The  returned *`SubClassID`* is used in the `.prepare_payment` call.


*`Example`:*

    $ncc_ubp->get_business_sub_class_id([
        "ID"             => "...",
        "PhoneNumber"     => "..."
    ]);


___


#### d). __*`.prepare_payment({MerchantID,BusinessID,Year,PhoneNumber,PaymentTypeID,SBPSubClassID},$headers)`*__
> Acceptable Parameters

|**Parameter**| **Type**| **Required**|**Description**
| :--- | :--- | :--- | :--- 
| *MerchantID*  | String|  `R` | Merchant Identifier
|*BusinessID* | String | `R` | License ID
| *Year* | Int | `R` |  2017
| *PhoneNumber* | String | `R` | PhoneNumber of the wallet
| *PaymentTypeID* | Int | `R` | 1
| *SBPSubClassID* | Int | `R` | ID from the ***getsbpsubclasses*** call
|

> The *`transactionID`* from the response is used in the *`.commit_payment`* call.


*`Example`:*

    $ncc_ubp->prepare_payment([
        "MerchantID"          => "...",
        "BusinessID"          => "...",
        "Year"                => "...",
        "PhoneNumber"         => "...",
        "PaymentTypeID"       => "...",
        "SBPSubClassID"       => "..."
    ]);


___



#### e). __*`.commit_payment({PhoneNumber,MerchantID,TransactionID,Pin,Year,AcceptedTnC,ContactPersonName,IDDocumentNumber,Telephone1,ZoneCode,WardCode,ZoneName,WardName,PlotNumber},$headers)`*__
> Acceptable Parameters

| **Parameter** | **Type** | **Required** |**Description** 
| :--- | :--- | :--- | :--- 
|*PhoneNumber* | String |`R`| Wallet PhoneNumber
|*MerchantID*  | String  |`R` |  Merchant identifier
|*TransactionID* | String |`R`|  Unique ID from the POST response
|*Pin* | String |`R`| Wallet Pin
|*Year* | Int | `R`| Year of business permit renewal
|*AcceptedTnC* |  Boolean |`R`| Accept terms and conditions, e.g. True
|*ContactPersonName*|String|`R` |  Contact Person
|*IDDocumentNumber* | String |`R`|  Number of the ID document
|*Telephone1*| String |`R` | Telephone1
|*Building*| String | - |            
|*Floor*| String| -  | Floor
|*RoomStallNumber*| String | - |  Room Stall Number
| *ZoneCode*| Int | `R`| Business location Zone Code
|*WardCode*| Int |`R`| Business location Ward Code
|*ZoneName* | String |`R` | Business location Zone Name
|*WardName* | String  | `R` | Business location Ward Name
|*PlotNumber* | String | `R` | Business location Plot Number
|

> The response parameters will contain a valid *`ReceiptNumber`* which denotes that
the transaction has been successfully commited and completed.


*`Example`:*

    $ncc_ubp->commit_payment([
        "PhoneNumber"         => "...",
        "MerchantID"          => "...",
        "TransactionID"       => "...",
        "Pin"                 => "...",
        "Year"                => "...",
        "AcceptedTnC"         => "...",
        "ContactPersonName"   => "...",
        "IDDocumentNumber"    => "...",
        "Telephone1"          => "...",
        "ZoneCode"            => "...",
        "WardCode"            => "...",
        "ZoneName"            => "...",
        "WardName"            => "...",
        "PlotNumber"          => "..."
    ]);


___
___
## B). *LAND RATES (`jambopay\ncc\LandRates()`)*

`Instantiate the class`
    
    $ncc_land_rates = new jambopay\ncc\LandRates();

##### *Available methods*
____  


### a). __*`.prepare_payment({PlotNumber,PhoneNumber,PaymentTypeID},$headers)`*__
> Acceptable Parameters

| **Parameter**  | **Type** |  **Required**  |**Description**
| :--- | :--- | :--- |  :---
| MerchantID | String  | `R` | Merchant identifier
| PlotNumber  |String  |`R`| Plot number
|PhoneNumber | String | `R`| Customers phone number
| PaymentTypeID |  Int    |   `R` | Mode of payment e.g. 1 for cash, etc
|

> The *`transactionID`* from the response is used in the *`.commit_payment`* call.

*`Example`:*

    $ncc_land_rates->prepare_payment([
        "PlotNumber"      => "...",
        "PhoneNumber"     => "...",
        "PaymentTypeID"   => "..."
    ]);



### b). __*`.commit_payment({TransactionID,Amount},$headers)`*__
> Acceptable Parameters

|**Parameter**  | **Type**  | **Required**   |**Description**
| :---- | :---- | :---- | :----
|MerchantID   |   String  |  `R`  |           Merchant identifier
|TransactionID  | String  |  `R`   |  Unique transaction reference number
|Amount     |     decimal |  `R`   |          transaction amount
|

> The response parameters will contain a valid *`ReceiptNumber`* which denotes that
the transaction has been successfully commited and completed.

*`Example`:*

    $ncc_land_rates->commit_payment([
            "TransactionID"   => "...",
            "Amount"          => "..."
    ]);

____
___
## C). *PARKING (`jambopay\ncc\Parking()`)* 

`Instantiate the class`
   
    $ncc_parking = new jambopay\ncc\Parking();

____
### **`DAILY PARKING`**    




##### *Available methods*
___

#### a). __*`.get_daily_parking_items ( headers )`*__
> This call pulls the required information to determine the amount to be paid.i.e Vehicle types and Zones.

*`Example`:*

    $ncc_parking->get_daily_parking_items();
___



#### b). __*`.prepare_payment({MerchantID,IsDaily,PlateNumber,PaymentTypeID,PhoneNumber,ZoneCodeID,ChargeID},$headers)`*__
> Acceptable Parameters

| **Parameter** |  **Type** |  **Required**   |**Description**
| :--- | :--- | :--- |  :---
|MerchantID  |    String  |  `R`      |      Merchant identifier
| IsDaily    |     bool  |    `R`      |       Whether parking is daily or not e.g 1 or 0; true or false
| PlateNumber    | string   | `R`       |      Car registration Number e.g. KDF888W
|PaymentTypeID |  Int   |    `R`     |        Mode of payment e.g. 1
| PhoneNumber  |   string  |  `R`      | Customer phone number
|ZoneCodeID   |   Int    |   `R`    |         Unique id describing the parking zone e.g.10
|ChargeID  |      Int  |     `R`     |        Unique id describing the parking charges e.g.10
|DurationID   |   Int   |     O    |          Unique id describing the parking duration
|

> The *`transactionID`* from the response is used in the *`.commit_payment`* call.


*`Example`:*

    $ncc_parking->prepare_payment([
        "MerchantID"      => "...",
        "IsDaily"         => "...",
        "PlateNumber"     => "...",
        "PaymentTypeID"   => "...",
        "PhoneNumber"     => "...",
        "ZoneCodeID"      => "...",
        "ChargeID"        => "..."
    ]);
____



#### c). __*`.commit_payment({TransactionID,Amount},$headers)`*__
> Acceptable Parameters

| **Parameter** |  **Type**|   **Required**   |**Description**
| :--- | :--- | :--- |  :---
| MerchantID   | String  |`R` |  Merchant identifier
| TransactionID  | String  |  `R` |Transaction id
| PhoneNumber  |   String  |  `R`  |  Client Phone Number
| PaidBy | String  |  `R`  |  Client Name
| Amount | Decimal |  `R`  | Amount paid for parking
|

> The response parameters will contain a valid *`ReceiptNumber`* which denotes that
the transaction has been successfully commited and completed.

*`Example`:*

    $ncc_parking->commit_payment([
        "TransactionID"   => "...",
        "Amount"          => "..."
    ]);


___
### **`SEASONAL PARKING`**  

##### *Available methods*
___


#### a). __*`.get_seasonal_parking_items ( headers )`*__
> This gets initialization data (fees and charges) to determine the amount to be paid. This includes: vehicle type and Parking duration.


*`Example`:*

    $ncc_parking->get_seasonal_parking_items([
        "authorization"    => JAMBOPAY_TOKEN, 
        "app_key"          => "..."
    ]);

___

#### b). __*`.prepare_payment({MerchantID,IsDaily, PlateNumber,PaymentTypeID,PhoneNumber,ZoneCodeID,ChargeID,DurationID},$headers)`*__
> Acceptable Parameters


| **Parameter** |  **Type** |  **Required**   |**Description**
| :--- | :--- | :--- |  :---
|  MerchantID   |   String  |  `R`   |  Merchant identifier
|IsDaily   |      Bool   |   `R`    |         Whether parking is daily or not
| PlateNumber |    String  |  `R`  |           Car plate no.
| PaymentTypeID |  Int   |    `R`    |         Mode of payment
|PhoneNumber   |  string  |  `R`    |         Customer phone number
|ZoneCodeID  |    Int    |   `R`   |          Unique id describing the parking zone
|ChargeID    |    Int    |   `R`    |         Unique id describing the parking charges
|DurationID   |   Int    |   `R`     |        Unique id describing the parking duration
|

> The *`transactionID`* from the response is used in the *`.commit_payment`* call.


*`Example`:*

    $ncc_parking->prepare_payment([
        "MerchantID"      => "...",
        "IsDaily"         => "...",
        "PlateNumber"     => "...",
        "PaymentTypeID"   => "...",
        "PhoneNumber"     => "...",
        "ZoneCodeID"      => "...",
        "ChargeID"        => "...",
        "DurationID"      => "..."
    ]);

___


#### c). __*`.commit_payment({MerchantID,TransactionID},$headers)`*__
> Acceptable Parameters

| **Parameter** |  **Type** |  **Required** |  **Description**
| :--- | :--- | :--- |  :---
| MerchantID  |    String   |  `R`   |          Merchant identifier
| TransactionID |  String |    `R` |            Transaction id
|
> The response parameters will contain a valid *`ReceiptNumber`* which denotes that
the transaction has been successfully commited and completed.


*`Example`:*

    $ncc_parking->commit_payment([
        "TransactionID"   => "...",
        "MerchantID"      => "..."
    ]);


___
___
## D). *SACCO PARKING (`jambopay\ncc\SaccoParking()`)*


`Instantiate the class`

    $ncc_sacco_parking = new jambopay\ncc\SaccoParking();

##### *Available methods*
____
### a). __*`.get_sacco_parking_items ( headers )`*__
> This call pulls the required information to determine the amount to be paid. This include Vehicle types and duration types.


*`Example`:*

    $ncc_sacco_parking->get_sacco_parking_items([
        "authorization" => JAMBOPAY_TOKEN, 
        "app_key"       => "..."
    ]);

___

### b). __*`.prepare_payment({SaccoName,Vehicles: [{VehicleType,Duration,RegistrationNumber}],PaymentTypeID},$headers)`*__
> Acceptable Parameters

| **Parameter**  | **Required**| **Type**|**Description**
| :--- | :--- | :--- |  :---
|SaccoName |`R`| String | name of sacco    
| Vehicles| ***Array fields*** 
| | VehicleType  | `R` |  int
||Duration |  `R`  |  int 
||RegistrationNumber | `R`| string  
|PaymentTypeID |`R` |  int  | Mode of payment e.g. 1 - cash
|

> The *`transactionID`* from the response is used in the *`.commit_payment`* call.

*`Example`:*

    $params = [       
                "SaccoName"  =>  "..." , 
                "Vehicles" =>  [
                    "vehicleType"       => "...",
                    "Duration"          => "...",
                    "RegistrationNumber" => "..."
                ], 
                "PaymentTypeID"    => "..." 
    ];

    $ncc_sacco_parking->prepare_payment($params);


___

### c). __*`.commit_payment({},$headers)`*__
> Acceptable Parameters

> The response parameters will contain a valid *`ReceiptNumber`* which denotes that
the transaction has been successfully commited and completed.

| **Parameter** |  **Type** |  **Required** |  **Description**
| :--- | :--- | :--- |  :---
| TransactionID |  String |    `R`             Transaction id
|

*`Example`:*

    $params = [       
                "TransactionID"   => "..." 
            ];

    $ncc_sacco_parking->commit_payment($params);



____
___
## D). *HOUSE RENT (`.house_rent`)*


`Instantiate the class`

    $ncc_house_rent = new jambopay\ncc\HouseRent();

##### *Available methods*
____


### a). __*`.get_estates ( headers )`*__
> This call gets a list of estates with houses and markets that belong to Nairobi County

*`Example`:*

    $ncc_house_rent->get_estates();

___

### b). __*`.get_residences ( { ID }, headers ))`*__
> This call gets the list of residences within an estate using the EstateID

*`Example`:*

    $ncc_house_rent->get_residences([
        "ID"          => "..."
    ]);
___

### c). __*`.prepare_payment({TransactionID,Names,Amount,Adjustment,Currentbalance,CustomerNames,LastBillMonth,MonthlyOtherCharges,MonthlyRent,OtherArrears,PhysicalAddress,RentArrears},$headers)`*__
> Acceptable Parameters

|**Output**  | **Type** | **Description**
| :--- | :--- |  :---
|TransactionID  |       string  |   Unique transaction reference number
|Names    |             string |    Tenant names
|Amount    |            decimal  |  Amount to pay
|Adjustment    |        decimal  |  any adjustment accrued in payment
| Currentbalance    |    decimal |   Current balance
| CustomerNames  |       string  |   Customer names
| LastBillMonth    |     DateTime |  Last month bill was paid
| MonthlyOtherCharges  | decimal  |  Other monthly charges
| MonthlyRent    |       decimal  |  Monthly rent
| OtherArrears    |      decimal  |  Other arrears
| PhysicalAddress  |     decimal  |  Physical address
| RentArrears     |      decimal |   Rent arrears
| RentDueDate   |        DateTime |  Rent due date
| UHN          |         string  |   Unique house number
|

> The *`transactionID`* from the response is used in the *`.commit_payment`* call.

*`Example`:*

    $ncc_house_rent->prepare_payment([
        "TransactionID"   => "...",
        "Names"           => "...",
        "Amount"          => "...",
        "Adjustment"      => "...",
        "Currentbalance"  => "...",
        "CustomerNames"   => "...",
        "LastBillMonth"   => "...",
        "MonthlyOtherCharges"     => "...",
        "MonthlyRent"     => "...",
        "OtherArrears"    => "...",
        "PhysicalAddress" => "...",
        "RentArrears"     => "...",
    ]);

___




### d). __*`.commit_payment({TransactionID,Amount},$headers)`*__
> Acceptable Parameters

| **Parameter**  | **Type** |  **Required**  | **Description**
| :--- | :--- | :--- |  :---
| TransactionID|   String |   `R`    |        Unique transaction reference number
| Amount   |       decimal  | `R`  |          Amount to pay
|
> The response parameters will contain a valid *`ReceiptNumber`* which denotes that
the transaction has been successfully commited and completed.

*`Example`:*

    $ncc_house_rent->commit_payment([
        "TransactionID"   => "...",
        "Amount"          => "..."
    ]);


____
___
## D). *E-CONSTRUCTION (`jambopay\ncc\EConstruction()`)*



`Instantiate the class`
    
    $ncc_e_construction = new jambopay\ncc\EConstruction();

##### *Available methods*

### a). __*`.prepare_payment({InvoiceNumber,PaymentTypeID,PhoneNumber,PaidBy},$headers)`*__
> Acceptable Parameters

|  **Parameter** |  **Type** |  **Required**   |**Description**
| :--- | :--- | :--- | :--- | --
| InvoiceNumber |  string  |  `R`| invoice number
| PaymentTypeID |  int   |  `R` | Mode of payment
| PhoneNumber | string | `R` | The client's phone number
| PaidBy | string | `R` | The name of the client
|
>   The *`transactionID`* from the response is used in the *`.commit_payment`* call.

*`Example`:*

    $ncc_e_construction->prepare_payment([
        "InvoiceNumber" => "...",
        "PaymentTypeID" => "..."
    ]);

___

### b). __*`.commit_payment({TransactionID,PhoneNumber},$headers)`*__
> Acceptable Parameters

| **Parameter** |  **Type** |  **Required** |  **Description**
| :--- | :--- | :--- | :--- |  :---
| TransactionID  | string   | `R`|Unique transaction reference number
| PhoneNumber  |   string  |  `R`   |   User phone number
|

> The response parameters will contain a valid *`ReceiptNumber`* which denotes that
the transaction has been successfully commited and completed.

*`Example`:*

    $ncc_e_construction->commit_payment([
        "TransactionID"   => "...",
        "PhoneNumber"      => "..."
    ]);

____
___
## E). *LIQUOR (`jambopay\ncc\Liquor()`)*
##### *Available methods*

`Instantiate the class`

    $ncc_liquor = new jambopay\ncc\Liquor();

### a). __*`.prepare_payment({ Date,LicenseNumber,PhoneNumber,PaymentTypeID},$headers)`*__
> Acceptable Parameters

|  **Parameter**  | **Type**  | **Required** |  **Description**
| :--- | :--- | :--- | :--- |  :---
|  Date     | DateTime  |`R` | date
| LicenseNumber |  String   | `R`| License number
| PhoneNumber  |   String  |  `R` |  Customer
| PaymentTypeID |  int?  | `R` |  Mode of payment
|

> The *`transactionID`* from the response is used in the *`.commit_payment`* call.

*`Example`:*

    $ncc_liquor->prepare_payment([
        "Date"            => "...",
        "LicenseNumber"   => "...",
        "PhoneNumber"     => "...",
        "PaymentTypeID"   => "..."
    ]);

___

### b). __*`.commit_payment({TransactionID},$headers)`*__
> Acceptable Parameters


|**Parameter** |**Type** |**Required** |**Description**
| :--- | :--- | :--- | :--- |  :---
| TransactionID |  String  |   `R`    | Unique transaction reference number
|


> The response parameters will contain a valid *`ReceiptNumber`* which denotes that
the transaction has been successfully commited and completed.

*`Example`:*

    $ncc_liquor->commit_payment([
        "TransactionID"   => "..."
    ]);



____
___
## E). *MISCELLANEOUS (`jambopay\ncc\Miscellaneous()`)*
##### *Available methods*


`Instantiate the class`
   
    $ncc_misc = new jambopay\ncc\Miscellaneous();



### a). __*`.prepare_payment({PhoneNumber,BillNumber,PaymentTypeID},$headers)`*__
> Acceptable Parameters

 | **Parameter** |  **Type** |  **Required**  | **Description**
 | :--- | :--- | :--- | :--- | :--- 
 | PhoneNumber   |  string   |  `R`  | Customer phone number
 | BillNumber  |    string  |   `R` | Unique Bill reference number gotten from county
 | PaymentTypeID  | int  |  `R` | Mode of payment
 |

> The *`transactionID`* from the response is used in the *`.commit_payment`* call.

*`Example`:*

    $ncc_misc->prepare_payment([
        "PhoneNumber"     => "...",
        "BillNumber"      => "...",
        "PaymentTypeID"   => "..."
    ]);
___

### b). __*`.commit_payment({TransactionID},$headers)`*__
> Acceptable Parameters

| **Parameter**  | **Type** |  **Required**  | **Description**
| :--- | :--- | :--- |  :---
|TransactionID |  String  |   `R`  | Unique transaction reference number
|

> The response parameters will contain a valid *`ReceiptNumber`* which denotes that
the transaction has been successfully commited and completed.

*`Example`:*

    $ncc_misc->commit_payment([
        "TransactionID"   => "..."
    ]);



____
___
# 2. __NAIROBI WATER PAYMENTS (`jambopay\NairobiWater()`)__



`Instantiate the class`

    $nairobi_water = new jambopay\NairobiWater();



## a). __*`.get_bill({AccountNumber},$headers)`*__
> This call pulls the applicable charges for Nairobi Water.


*`Example`:*

    $nairobi_water->get_bill([
        "AccountNumber"   => "..."
    ]);

___

## b). __*`.prepare_payment({AccountNumber,PhoneNumber,Names,Amount,PaymentTypeID},$headers)`*__
> Acceptable Parameters

|**Parameter**|   **Type** |  **Required** |  **Description**
| :--- | :--- | :--- |  :-------
| AccountNumber |  String  |  `R` | Nairobi Water customer account number
|PhoneNumber| String| `R`  |User phone number
|Names | String | `R` | Customer account names
|Amount | decimal | `R`  | Transaction amount
|PaymentTypeID | int? | `R` | Mode of payment
|

> The *`transactionID`* from the response is used in the *`.commit_payment`* call.

*`Example`:*

    $nairobi_water->prepare_payment([
        "AccountNumber"   => "...",
        "PhoneNumber"     => "...",
        "Names"           => "...",
        "Amount"          => "...",
        "PaymentTypeID"   => "..."
    ]);


___

## c). __*`.commit_payment({TransactionID},$headers)`*__
> Acceptable Parameters

| **Parameter** | **Type** |  **Required**  |**Description**
| :--- | :--- | :--- |  :---
| TransactionID | String |   `R`  | Unique transaction reference number
|

> The response parameters will contain a valid *`ReceiptNumber`* which denotes that
the transaction has been successfully commited and completed.

*`Example`:*

    $nairobi_water->commit_payment([
        "TransactionID"   => "..."
    ]);


____
___
# 3. __NATIONAL HOSPITAL INSURANCE FUND PAYMENTS (`jambopay\NHIF()`)__

`Instantiate the class`

    $nhif = new jambopay\NHIF();


## a). __*`.prepare_payment({AccountNumber,IsCorporate,PhoneNumber,Amount,PaymentTypeID},$headers)`*__
> Acceptable Parameters

| **Parameter**  | **Type** |  **Required**  | **Description**
| :--- | :--- | :--- | :--- 
| AccountNumber | String | `R` | Customer account number at NHIF i.e. member number
| IsCorporate |  Bool |  `R` | Whether transaction is corporate or individual, use **False**
| PhoneNumber |  String  |   `R` |Customers phone number
| Penalty|  Decimal  |  *O*  |Amount to pay as penalty.
| Amount|  Decimal  |  `R`  | Transaction amount.
| PaymentTypeID |  Int | `R` |  Mode of payment e.g. 1 for cash, etc
|

> The *`transactionID`* from the response is used in the *`.commit_payment`* call.


*`Example`:*

    $nhif->prepare_payment([
        "AccountNumber"   => "...",
        "IsCorporate"     => "...",
        "PhoneNumber"     => "...",
        "Amount"          => "...",
        "PaymentTypeID"   => "..."
    ]);


___
## b). __*`.commit_payment({TransactionID,PhoneNumber,Amount},$headers)`*__
> Acceptable Parameters

|**Parameter** | **Type** | **Required** | **Description**
| :--- | :--- | :--- |  :---
| TransactionID | String | `R`  | Unique transaction reference number
| PhoneNumber |    String  | `R` | Customers phone number
| Amount  |  decimal |   `R`  |  Transaction amount
|

> The response parameters will contain a valid *`ReceiptNumber`* which denotes that
the transaction has been successfully commited and completed.


*`Example`:*

    $nhif->commit_payment([
        "TransactionID"   => "...",
        "PhoneNumber"     => "...",
        "Amount"          => "..."
    ]);


____
___
# 4. __KASNEB PAYMENTS (`jambopay\Kasneb()`)__


`Instantiate the class`
    
    $kasneb = new jambopay\Kasneb();


## a). __*`.prepare_payment({PhoneNumber,BillNumber,PaidBy,PaymentTypeID},$headers)`*__
> Acceptable Parameters

|  **Parameter**   |  **Description**
| :--- | :---     
| PhoneNumber | The phone number of the individual making the payment
|BillNumber   |               The bill number or invoice number to which payment is to be made
|PaidBy       |               The names of the individual making the payment
|PaymentTypeID   |            The type of payment being made. For all agents we request this be defaulted to 1 which stands for cash transactions
|

> The *`transactionID`* from the response is used in the *`.commit_payment`* call.

*`Example`:*

    $kasneb->prepare_payment([
        "PhoneNumber"     => "...",
        "BillNumber"      => "...",
        "PaidBy"          => "...",
        "PaymentTypeID"   => "..."
    ]);
___


## b). __*`.commit_payment({PhoneNumber,TransactionID,ExaminationCenterID,Tendered},$headers)`*__
> Acceptable Parameters

| **Parameter** | **Description**
| :--- | :--- 
| PhoneNumber | The phone number of the individual making the payment
| TransactionID | This is the unique identifier for the payment to be processed. This value comes from the previous output from a property with the same name
| ExaminationCenterID |  Null able parameter. Only applies to payments that have an exam center applied to them.
| Tendered  | This is an optional parameter denoting the total amount of money given by the client. It is either same to or more than the Amount field and can be used to compute change owed to the client
|

> The response parameters will contain a valid *`ReceiptNumber`* which denotes that
the transaction has been successfully commited and completed.

*`Example`:*

    $kasneb->commit_payment([
        "TransactionID"       => "...",
        "PhoneNumber"         => "...",
        "Tendered"            => "...",
        "ExaminationCenterID" => "..."
    ]);

____
___
# 5. __JAMBOPAY WALLET PAYMENTS (`jambopay\Wallet()`)__


`Instantiate the class`

    $wallet = new jambopay\Wallet();


## a). __*`.prepare_payment({PhoneNumber,Amount,PaymentTypeID},$headers)`*__
> Acceptable Parameters

| **Parameter**  | **Type** |  **Required** |  **Description**
| :--- | :--- | :--- |  :---
|PhoneNumber | string | `R`|  User phone number
|Amount | decimal |   `R`  | Transaction amount
|PaymentTypeID  | Int | `R`|  Mode of payment.e.g. 1 for cash, 2 for cheque, etc
|

> The *`transactionID`* from the response is used in the *`.commit_payment`* call.

*`Example`:*

    $wallet->prepare_payment([
        "PhoneNumber"   => "...",
        "Amount"        => "...",
        "PaymentTypeID" => "..."
    ]);

___

## b). __*`.commit_payment({TransactionID,PhoneNumber},$headers)`*__
> Acceptable Parameters

| **Parameter** | **Type** | **Required**  |**Description**
| :--- | :--- | :--- |  :---
| TransactionID |  String   | `R` | Unique transaction reference number
| PhoneNumber | String  | `R`  | User phone number
|

> The response parameters will contain a valid *`ReceiptNumber`* which denotes that
the transaction has been successfully commited and completed.


*`Example`:*

    $wallet->commit_payment([
        "TransactionID"   => "...",
        "PhoneNumber"     => "..."
    ]);


___
____

# 6. __JAMBOPAY PAYMENT REPORTS (`jambopay\Reports(reporting_stream [ e.g. parking, saccoparking, rent,nhif,ubp])`)__


>>> INITIALIZE THE QUERY OBJECT

##### e.g
`For Parking`
    
    $parking_report = new jambopay\Reports("parking");

---
## a). __*`.get_transactions({},$headers)`*__
> Acceptable parameters


| **Parameter**  | **Type** | **Required** |**Description**
| :--- | :--- | :--- | :--- 
| PlateNumber |string |    *O*  |            Id of the business
|Index | int | *O* | Zero based index of the pages of the transactions to return- each page has 30 transactions
|TransactionID  |     string  |   *O*   |           Unique transaction reference number
|  StartDate     |      Date    |   *O*    |          Least date to fetch transactions from
|  EndDate    |         Date  |     *O*   |           Latest date to fetch transactions
|  UserID     |         string  |   *O*   |           Unique user id
|  TransactionStatus |  byte   |    *O*    |          State of the transaction (prepared-0 or completed-1)
|


`Sample Usage ` ***( for ncc parking )***

    $parking_report->get_transactions( );


____
___

# 7. __KENYA POWER & LIGHTING Co. PAYMENTS (`jambopay\KPLC()`)__

`Instantiate the class`

    $kplc = new jambopay\KPLC();


## a). __*`.prepare_payment({PhoneNumber,Amount,PaymentTypeID,MeterNumber,MerchantID},$headers)`*__
> Acceptable Parameters


| **Parameter**  | **Type** |  **Required** |  **Description**
| :--- | :--- | :--- |  :---
|PhoneNumber | string | `R`|  User phone number
|Amount | decimal |   `R`  | Transaction amount
|PaymentTypeID  | Int | `R`|  Mode of payment.e.g. 1 for cash, 2 for cheque, etc
| MeterNumber | Int | `R` | The client's meter number
| MerchantID | Int | `R` | '`KPLC`' in this case
|

> The *`transactionID`* from the response is used in the *`.commit_payment`* call.

*`Example`:*

    $kplc->prepare_payment([
        "PhoneNumber"   => "...",
        "Amount"        => "...",
        "PaymentTypeID" => "...",
        "MeterNumber"   => "...",
        "MerchantID"    => "KPLC"
    ]);

___


## b). __*`.commit_payment({TransactionID,PhoneNumber,Tendered,Pin},$headers)`*__

> Acceptable Parameters

| **Parameter** | **Type** | **Required**  |**Description**
| :--- | :--- | :--- |  :---
| TransactionID |  String   | `R` | Unique transaction reference number
| PhoneNumber | String  | `R`  | User phone number
| Tendered |Int  | `R`  | The amount that has been given by the client
| Pin | String  | `R`  | The initiator's access password/PIN
|

> The response parameters will contain a valid *`ReceiptNumber`* which denotes that
the transaction has been successfully commited and completed.


*`Example`:*

    $kplc->commit_payment([
        "TransactionID"   => "...",
        "PhoneNumber"     => "...",
        "Tendered"        => "...",
        "Pin"             => "..."
    ]);

____
___
# 8. __OFFSTREET PARKING PAYMENTS (` jambopay\OffStreetParking(bool liveServer = false)`)__

> This endpoint requires initialization before it's use.
The main purpose for this is to specify whether or not to use the live endpoint.

`To initialize the endpoint use`

    $offstreet_parking = new jambopay\OffStreetParking(`use_live_server`);
>
### `Either`

    $offstreet_parking = new jambopay\OffStreetParking(true);
>
###    `or`
>
    $offstreet_parking = new jambopay\OffStreetParking(false);



## a). __*`.validate_attendant({PhoneNumber},$headers)`*__
> This method helps to check the validity of an attendant by their telephone number

*`Example:`*
    
    $offstreet_parking->validate_attendant([
        PhoneNumber => "..."
    ]);

____

## b). __*`.book_vehicle({PlateNumber,attPhoneNumber},$headers)`*__
> This method helps to book in a vehicle to the system by the plate number

*`Example:`*
    
    $offstreet_parking->book_vehicle([
        "PlateNumber"     => "...",
        "attPhoneNumber"  => "..."
    ]);
    
____


___
# 9. __STARTIMES PAYMENTS (`jambopay\Startimes()`)__


`Instantiate the class`

    **$startimes = new jambopay\Startimes();**


## a). __*`.prepare_payment({PhoneNumber,Amount,PaymentTypeID,SmartCardCode,CustomerCode},$headers)`*__
> Acceptable Parameters

| **Parameter**  | **Type** |  **Required** |  **Description**
| :--- | :--- | :--- |  :---
|PhoneNumber | string | `R`|  User phone number
|Amount | decimal |   `R`  | Transaction amount
|PaymentTypeID  | Int | `R`|  Mode of payment.e.g. 1 for cash, 2 for cheque, etc
| SmartCardCode | Int | `R` | The client's smartcard number
| CustomerCode | Int | `R` | The designated customer code
|

> The *`transactionID`* from the response is used in the *`.commit_payment`* call.

*`Example`:*

    $startimes->prepare_payment([
        "PhoneNumber"   => "...",
        "Amount"        => "...",
        "PaymentTypeID" => "...",
        "SmartcardCode" => "...",
        "CustomerCode"  => "..."
    ]);

___


## b). __*`.commit_payment({TransactionID,PhoneNumber,Tendered},$headers)`*__
> Acceptable Parameters

| **Parameter** | **Type** | **Required**  |**Description**
| :--- | :--- | :--- |  :---
| TransactionID |  String   | `R` | Unique transaction reference number
| PhoneNumber | String  | `R`  | User phone number
| Tendered |Int  | `R`  | The amount that has been given by the client
|

> The response parameters will contain a valid *`ReceiptNumber`* which denotes that
the transaction has been successfully commited and completed.


*`Example`:*

    $startimes->commit_payment([
        "TransactionID"   => "...",
        "PhoneNumber"     => "...",
        "Tendered"        => "..."
    ]);


----
----


# **GLOBAL METHODS**


`Instantiate the class`

    $global = new jambopay\JambopayGeneric('stream');

## __*`$global->prepare_payment({},$headers)`*__

## __*`$global->commit_payment({},$headers)`*__

The above methods can be invoked directly for any payment channel (`apart from` **`offstreetparking`**) as long as the relevant payment *stream* is passed.

#### Below is a table of the relevant payment streams
| **Target** | **Stream**
| :--- |  :---
| E-Construction | `econstruction`
| House Rent | `rent`
| Jambopay Wallet | `wallet`
| KASNEB  | `kasneb`
| Kisumu Water & Sewerage | `kiwasco`
| Kenya Power & Lighting | `kplc`
| Land Rates | `landrate`
| Liquor | `liquor`
| Miscellaneous | `misc`
| Nairobi Water | `nairobiwater`
| NHIF | `nhif`
| NCC - OffStreet Parking | `offstreetparking`
| Parking | `parking`
| Sacco Parking | `saccoparking`
| Unified Business Permit | `sbp`
|

#### Below is a table of applicable MerchantID's
| **Target** | **MerchantID**
| :--- |  :---
| Nairobi County | `NCC`
| Trans Nzoia County | `TNC`
*`An example sacco_parking prepare_payment request`*

    $params = [   
            "Stream"      =>"saccoparking"
            "SaccoName"   => "" , 
            "Vehicles"    => [ 
                    "vehicleType"        => "",
                    "Duration"           => "",
                    "RegistrationNumber" => ""
                ] 
            ], 
            "PaymentTypeID" => ""            
        ];

>

    $JamboPay_saccoparking = new jambopay\JambopayGeneric('saccoparking');    
    $JamboPayWildCard->prepare_payment($params);

#### **OR**

    $JamboPayWildCard_any = new jambopay\JambopayGeneric();
    $JamboPayWildCard_any->prepare_payment($params);

`When using the latter, ensure that you have a "Stream" defined in the parameters object that you pass into the method rather than passing it to the constructor.`